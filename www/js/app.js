var Encabezado = React.createClass({
    render: function () {
        return (
          <header className="bar bar-nav">
          <a href="#" className={"icon icon-left-nav pull-left" + (this.props.back==="true"?"":" hidden")}></a>
            <h1 className="title">{this.props.text}</h1>
          </header>
        );
    }
});

var BarraBusqueda = React.createClass({
    getInitialState: function() {
        return {buscaTecla: ""};
     },
    manejaBusqueda: function(evento) {
        var buscaTecla = evento.target.value;
        this.setState({buscaTecla : buscaTecla });
        this.props.manejaBusqueda(buscaTecla);
    },
    render: function () {
        return (
          <div className="bar bar-standard bar-header-secondary">
            <input type="search" value={this.state.symbol} onChange={this.manejaBusqueda}/>
            </div>
        );
    }
});

var ElementoListaEmpleados = React.createClass({
     render: function () {
         return (
                <li className="table-view-cell media">
                  <a href={"#empleados/" + this.props.empleado.id}>
                     <img className="media-object small pull-left" src={"pics/" + this.props.empleado.nombre + "_"+this.props.empleado.apellido + ".jpg"}/>
                   {this.props.empleado.nombre} {this.props.empleado.apellido}
                   <p>{this.props.empleado.cargo}</p>
                  </a>
                </li>
                );
         }
});


var ListaEmpleados = React.createClass({
    render: function () {
      var elementos = this.props.empleados.map(function (empleado){
        return (
            <ElementoListaEmpleados key={empleado.id} empleado={empleado} />
         );
      });
    return (
            <ul className="table-view">
                {elementos}
            </ul>
        );
    }
});

var PaginaPrincipal = React.createClass({
   getInitialState: function() {
         return {empleados: []}
   },
    manejaBusqueda:function(tecla) {
             this.props.service.encuentraPorNombre(tecla).done(function(resultado){
                  this.setState({buscaTecla: tecla, empleados: resultado});
  }.bind(this));
     },
    render: function () {
        return (
            <div>
                <Encabezado text="Directorio Empleados" back="false"/>
                <BarraBusqueda manejaBusqueda={this.manejaBusqueda}/>
                <div className="content">
                    <ListaEmpleados empleados={this.state.empleados}/>
                </div>
            </div>
        );
    }
});


var PaginaEmpleado = React.createClass({
    getInitialState: function() {
        return {empleado: {}};
    },
    componentDidMount: function() {
        this.props.service.encuentraPorId(this.props.empleadoId).done(function(resultado) {
            this.setState({empleado: resultado});
        }.bind(this));
    },
    render: function () {
        return (
            <div>
                <Encabezado text="Empleado" back="true"/>
                <div className="card">
                     <ul className="table-view">
                     <li className="table-view-cell media">
                         <img className="media-object big pull-left" src={"pics/"+this.state.empleado.nombre + "_"+this.state.empleado.apellido +".jpg"}/>
                         <h1>{this.state.empleado.nombre} {this.state.empleado.apellido}</h1>
                         <p>{this.state.empleado.cargo}</p>
                     </li>
                     <li className="table-view-cell media">
                         <a href={"tel:" + this.state.empleado.telefonoOficina} className="push-right">
                                <span className="media-object pull-left icon icon-call"></span>
                                <div className="media-body">
                                Call Office
                                    <p>{this.state.empleado.telefonoOficina}</p>
                                </div>
                            </a>
                        </li>
                        <li className="table-view-cell media">
                            <a href={"tel:" + this.state.empleado.telefonoMovil} className="push-right">
                                <span className="media-object pull-left icon icon-call"></span>
                                <div className="media-body">
                                Call Mobile
                                    <p>{this.state.empleado.telefonoMovil}</p>
                                </div>
                            </a>
                        </li>
                        <li className="table-view-cell media">
                            <a href={"sms:" + this.state.empleado.telefonoMovil} className="push-right">
                                <span className="media-object pull-left icon icon-sms"></span>
                                <div className="media-body">
                                SMS
                                    <p>{this.state.empleado.telefonoMovil}</p>
                                </div>
                            </a>
                        </li>
                        <li className="table-view-cell media">
                            <a href={"mailto:" + this.state.empleado.email} className="push-right">
                                <span className="media-object pull-left icon icon-email"></span>
                                <div className="media-body">
                                Email
                                    <p>{this.state.empleado.email}</p>
                                </div>
                            </a>
                        </li>
                        </ul>
                    </div>
            </div>
        );
    }
});

enrutar.agregaRuta('', function() {
    React.render(
        <PaginaPrincipal service={empleadoServicio}/>,
        document.body
    );
});

enrutar.agregaRuta('empleados/:id',function(id){
  React.render(
     <PaginaEmpleado empleadoId={id} service={empleadoServicio}/>,
     document.body
    );
});

 enrutar.inicia();
